<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'namespace' => 'Api',
], function () {
    //API leitura de arquivos qware (STA)
    Route::get('/unidade/empenho/{ano}/{data?}', 'UnidadeController@getUnidadesComEmpenhos');
    Route::get('/unidade/rp/{data?}', 'UnidadeController@getUnidadesComRP');
    Route::get('/ler/unidade', 'UnidadeController@ler');
    Route::get('/ler/orgao', 'OrgaoController@ler');
    Route::get('/ler/credor', 'CredorController@ler');
    Route::get('/ler/planointerno', 'PlanointernoController@ler');
    Route::get('/ler/empenho', 'EmpenhoController@ler');
    Route::get('/ler/empenhodetalhado', 'EmpenhodetalhadoController@ler');
//    Route::get('/ler/rp', 'RpController@ler');
    Route::get('/ler/ordembancaria/', 'OrdembancariaController@ler');
    Route::get('/ler/dochabil', 'DocHabilController@ler');
    Route::get('/ler/saldocontabil', 'SaldosContabeisController@ler');

    //API Consulta Banco de Dados
    Route::get('/empenho/{dado}', 'EmpenhoController@buscaEmpenhoPorNumero');
    Route::get('/empenho/ano/{ano}/ug/{ug}/', 'EmpenhoController@buscaEmpenhoPorAnoUg');
    Route::get('/empenho/ano/{ano}/ug/{ug}/dia/{data?}', 'EmpenhoController@buscaEmpenhoPorDiaUg');
    Route::get('/rp/ug/{ug}/', 'RpController@buscaRpPorUg');
    Route::get('/rp/ug/{ug}/dia/{data?}', 'RpController@buscaRpPorDiaUg');
    Route::get('/empenhodetalhado/{dado}', 'EmpenhodetalhadoController@buscaEmpenhodetalhadoPorNumeroEmpenho');
    Route::get('/ordembancaria/favorecido/{dado}', 'OrdembancariaController@buscaOrdembancariaPorCnpj');
    Route::get('/ordembancaria/ano/{ano}/ug/{ug}', 'OrdembancariaController@buscaOrdembancariaPorAnoUg');
    Route::get('/centrocusto/mesref/{mesref}/ug/{ug}', 'CentroCustoController@buscaCentroCustoPorMesrefUg');
    Route::get('/saldocontabil/ano/{ano}/ug/{unidade}/gestao/{gestao}/contacontabil/{conta_contabil}/contacorrente/{conta_corrente}',
        'SaldosContabeisController@retornaSaldoPorUGGestaoContacontabilContacorrenteAno');
    Route::get('/saldocontabil/ano/{ano}/ug/{unidade}/gestao/{gestao}/contacontabil/{conta_contabil}',
        'SaldosContabeisController@retornaSaldoPorUGGestaoContacontabilAno');
    Route::get('/saldocontabil/ano/{ano}/ug/{unidade}/gestao/{gestao}',
        'SaldosContabeisController@retornaSaldoPorUGGestaoAno');

    Route::get('/saldocontabil/ano/{ano}/orgao/{orgao}',
        'SaldosContabeisController@retornaSaldoPorOrgaoAno');


    Route::get('/estrutura/orgaossuperiores', 'EstruturaController@buscaOrgaosSuperiores');
    Route::get('/estrutura/orgaos', 'EstruturaController@buscaOrgaos');
    Route::get('/estrutura/unidades', 'EstruturaController@buscaUnidades');
    Route::get('/estrutura/naturezadespesas', 'EstruturaController@buscaNaturezadespesas');


    Route::get('/daas/empenhos/d1/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@empenhosD1UnidadeAno');
    Route::get('/daas/empenhos/carga/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@empenhosCargaUnidadeAno');

    Route::get('/daas/itensempenhos/d1/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@itensEmpenhosD1UnidadeAno');
    Route::get('/daas/itensempenhos/carga/{unidade}/{documento_ano}/{meses}', 'v1\AcessaTGDaasController@itensEmpenhosCargaUnidadeAno');

    Route::get('/daas/saldocontabil/d1/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@saldoContabilD1UnidadeAno');
    Route::get('/daas/saldocontabil/carga/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@saldoContabilCargaUnidadeAno');

    Route::get('/daas/orgaos/carga', 'v1\AcessaTGDaasController@orgaos');
    Route::get('/daas/unidades/carga', 'v1\AcessaTGDaasController@unidades');
    Route::get('/daas/naturezasubitens/carga', 'v1\AcessaTGDaasController@naturezaSubitens');

    Route::get('/jdbc/executaclasse/{tipo}', 'v1\AcessaTGDaasController@execJdbcClass');

    Route::get('/contrato/unidades', 'v1\ContratosCConController@buscaUnidades');

    Route::get('/leitura/arquivos_daas', 'v1\ContratosCConController@criaJobsLeituraArquivos');
    Route::get('/leitura/arquivos_daas_url', 'v1\ContratosCConController@executaLeituraArquivosViaUrl');

    Route::get('/efdreinf/ug/{unidade}/gestao/{gestao}/competencia/{competencia}','v1\EfdreinfController@buscarDeducoesPorCompetencia');

});


