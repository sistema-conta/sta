<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Naturezasubitens extends Model
{
    protected $table = 'naturezasubitens';

    public function buscaNaturezaDespesas()
    {
        return $this->all();
    }
}
