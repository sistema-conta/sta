<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $table = 'unidades';

    public function buscaUnidade()
    {
        $retorno = [];

        $unidades = $this->whereIn('funcao', ['1', '3'])
            ->orderBy('codigo')
            ->get();

        foreach ($unidades as $unidade) {

            $orgao = Orgao::where('codigo', $unidade->orgao)
                ->first();

            if(isset($orgao->gestao)) {
                $retorno[] = [
                    'orgao' => $unidade->orgao,
                    'codigo' => $unidade->codigo,
                    'gestao' => $orgao->gestao,
                    'funcao' => $this->buscaFuncao($unidade->funcao),
                    'nome' => strtoupper($unidade->nome),
                    'nomeresumido' => strtoupper($unidade->nomeresumido)
                ];
            }
        }

        return $retorno;
    }

    private function buscaFuncao(int $funcao)
    {
        $retorno = '';

        switch ($funcao) {
            case 1:
                $retorno = "Executora";
                break;
            case 2:
                $retorno = "Credora";
                break;
            case 3:
                $retorno = "Controle";
                break;
        }

        return $retorno;
    }

}
