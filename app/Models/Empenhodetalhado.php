<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empenhodetalhado extends Model
{
    protected $table = 'empenhodetalhado';

    protected $fillable = [
        'ug',
        'gestao',
        'numeroli',
        'numitem',
        'subitem',
        'quantidade',
        'valorunitario',
        'valortotal',
        'descricao',
    ];

    function empenho() {
        return $this->belongsTo(Empenho::class, 'num_lista', 'numeroli');
    }

}
