<?php

namespace App\Models\Daas\AcessaTG;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrgaosDaas extends Model
{

    public function buscaOrgaos()
    {
        $sql = "SELECT
                    DISTINCT
                    CASE
                        WHEN os.co_orgao_supe = o.co_orgao THEN ''
                        ELSE os.co_orgao_supe
                    END as orgao_superior,
                    o.co_orgao as codigo,
                    g.co_gestao as gestao,
                    o.no_orgao as nome
                FROM wd_orgao o
                LEFT JOIN wd_orgao_supe os on o.id_orgao_supe = os.id_orgao_supe
                LEFT JOIN wd_gestao g on o.id_gestao_prin = g.id_gestao
                WHERE
                    (o.id_tp_admin_orgao in (1,2,3,4,5,6,7,8)) and
                    (g.co_gestao <> '-7' and g.co_gestao <> '-8' and g.co_gestao <> '-9')
                ORDER BY
                    o.co_orgao";

        return DB::connection('odbc-dwtg')
            ->select($sql);
    }

}
