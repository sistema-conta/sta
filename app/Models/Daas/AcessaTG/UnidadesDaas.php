<?php

namespace App\Models\Daas\AcessaTG;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnidadesDaas extends Model
{

    public function buscaUnidades()
    {
        $sql = "SELECT
                    DISTINCT
                    ug.co_ug codigo,
                    CASE
                        WHEN ug.co_cnpj_ug='-8' THEN '00000000000000'
                        WHEN ug.co_cnpj_ug='-7' THEN '00000000000000'
                        WHEN ug.co_cnpj_ug='-9' THEN '00000000000000'
                        WHEN ug.co_cnpj_ug='' THEN '00000000000000'
                        ELSE
                        LPAD(ug.co_cnpj_ug,14,'0')
                    END as cnpj,
                    CASE
                        WHEN ug.id_funcao_ug = 0 THEN '3'
                        ELSE ug.id_funcao_ug
                    END as funcao,
                    ug.no_ug as nome,
                    ug.sg_ug as nomeresumido,
                    o.co_orgao as orgao,
                    m.id_uf as uf
                FROM wd_ug ug
                LEFT JOIN wd_municipio m ON ug.id_municipio_ug = m.id_municipio
                LEFT JOIN wd_orgao o ON ug.id_orgao_ug = o.id_orgao
                LEFT JOIN wd_gestao g on o.id_gestao_prin = g.id_gestao
                WHERE
                    ug.id_funcao_ug IN (0,1,2) and
                    ug.in_ativa = 'SIM' and
                    (o.id_tp_admin_orgao in (1,2,3,4,5,6,7,8)) and
                    (g.co_gestao <> '-7' and g.co_gestao <> '-8' and g.co_gestao <> '-9')
                ORDER BY
                    ug.co_ug";

        return DB::connection('odbc-dwtg')
            ->select($sql);
    }

}
