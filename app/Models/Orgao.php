<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orgao extends Model
{
    protected $table = 'orgao';

    protected $hidden = [
        "id",
        'created_at',
        'updated_at'
    ];


    public function buscaTodosOrgaos()
    {

        $retorno = [];
        $orgaos = $this->all();

        foreach ($orgaos as $orgao) {
            $retorno[] = [
                "orgao_superior" => ($orgao->orgao_superior == '') ? $orgao->codigo : $orgao->orgao_superior,
                "codigo" => $orgao->codigo,
                "gestao" => $orgao->gestao,
                "nome" => strtoupper($orgao->nome)
            ];
        }

        return $retorno;

    }


}
