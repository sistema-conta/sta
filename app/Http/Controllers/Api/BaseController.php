<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function buscaArquivos()
    {

        $path = config('app.path_pendentes');

        if (is_dir($path)) {
            $diretorio = dir($path);
            while (($a = $diretorio->read()) !== false) {
                $arquivos[] = $a;
            }
            $diretorio->close();
        } else {
            echo 'A pasta inexistente.';
        }
        asort($arquivos);

        $key = array_search('processados', $arquivos);
        if($key!==false){
            unset($arquivos[$key]);
        }
        $key = array_search('.', $arquivos);
        if($key!==false){
            unset($arquivos[$key]);
        }
        $key = array_search('..', $arquivos);
        if($key!==false){
            unset($arquivos[$key]);
        }


        $nomearquivo = [];

        foreach ($arquivos as $arq) {
            $arq1 = explode('.', $arq);
            if ($arq1[1] == 'TXT') {
                $nomearquivo[] = [
                    'nomearquivo' => $arq1[0],
                    'case' => 0
                ];
            }

            if ($arq1[1] == 'txt') {
                $nomearquivo[] = [
                    'nomearquivo' => $arq1[0],
                    'case' => 1
                ];
            }

        }

        return $nomearquivo;
    }

    public function moverArquivoProcessado(string $namedestino, string $nome, string $extensao)
    {
        if (copy($nome . $extensao, $namedestino . ".LIDO".$extensao)) {
            unlink($nome . $extensao);
        }
    }




}
