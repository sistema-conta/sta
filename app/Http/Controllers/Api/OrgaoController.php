<?php

namespace App\Http\Controllers\Api;

use App\Models\Orgao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrgaoController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 5) == 'orgao' or substr($nome['nomearquivo'], 6, 5) == 'orgao') {
                    $dados = $this->lerArquivo($nome['nomearquivo'], $nome['case']);

                    foreach ($dados as $e) {
                        $busca = $this->buscaOrgao($e['codigo']);
                        if (!isset($busca->codigo)) {
                            $novo_orgao = new Orgao;
                            $novo_orgao->orgao_superior = ($e['orgao_superior'] == '00000') ? '' : $e['orgao_superior'];
                            $novo_orgao->codigo = $e['codigo'];
                            $novo_orgao->gestao = $e['gestao'];
                            $novo_orgao->nome = $e['nome'];
                            $novo_orgao->save();
                        }else{
                            $busca->orgao_superior = ($e['orgao_superior'] == '00000') ? '' : $e['orgao_superior'];
                            $busca->gestao = $e['gestao'];
                            $busca->nome = $e['nome'];
                            $busca->save();
                        }
                    }
                }

            }

            $ok = 'Órgãos lidos.';
        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function buscaOrgao($codigo)
    {

        $retorno = Orgao::where('codigo', $codigo)
            ->first();

        return $retorno;

    }

    public function lerArquivo($nomeaquivo,$case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if($case == 0){
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if($case == 1){
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) break;

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != FALSE) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);


        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'GR-ORGAO') {
                    $credor[$i]['codigo'] = str_pad(substr($valor, 0, 5), 5, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-NO-ORGAO') {
                    $credor[$i]['nome'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-CO-ORGAO-VINCULACAO') {
                    $credor[$i]['orgao_superior'] = str_pad(substr($valor, 0, 5), 5, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-CO-GESTAO-PRINCIPAL') {
                    $credor[$i]['gestao'] = str_pad(substr($valor, 0, 5), 5, "0", STR_PAD_LEFT);
                }
            }

            $i++;
        }

        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino,$name,$extref);

        $this->moverArquivoProcessado($namedestino,$name,$exttxt);

        return $credor;
    }

}
