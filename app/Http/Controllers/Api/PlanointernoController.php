<?php

namespace App\Http\Controllers\Api;

use App\Models\Planointerno;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanointernoController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'pi' or substr($nome['nomearquivo'], 6, 2) == 'pi') {
                    $planointerno = $this->lerArquivo($nome['nomearquivo'],$nome['case']);

                    foreach ($planointerno as $e) {
                        $busca = $this->buscaPi($e['codigo']);
                        if (!isset($busca->codigo)) {
                            $novo_listaempenho = new Planointerno;
                            $novo_listaempenho->codigo = $e['codigo'];
                            $novo_listaempenho->descricao = $e['descricao'];
                            $novo_listaempenho->save();
                        }
                    }
                }

            }

            $ok = 'Planos Internos Lidos.';
        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    protected function buscaPi($codigo)
    {

        $planointerno = Planointerno::where('codigo', $codigo)
            ->first();

        return $planointerno;

    }

    public function lerArquivo($nomeaquivo,$case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if($case == 0){
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if($case == 1){
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) break;

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != FALSE) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);


        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'IT-CO-PLANO-INTERNO') {
                    $planointerno[$i]['codigo'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-NO-PLANO-INTERNO') {
                    $planointerno[$i]['descricao'] = strtoupper(utf8_encode($valor));
                }
            }

            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino,$name,$extref);

        $this->moverArquivoProcessado($namedestino,$name,$exttxt);

        return $planointerno;
    }

}
