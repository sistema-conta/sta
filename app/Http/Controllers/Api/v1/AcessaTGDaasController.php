<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\EmpenhoController;
use App\Http\Controllers\Api\EmpenhodetalhadoController;
use App\Http\Controllers\Api\OrgaoController;
use App\Http\Controllers\Api\SaldosContabeisController;
use App\Http\Controllers\Api\UnidadeController;
use App\Models\Credor;
use App\Models\Daas\AcessaTG\EmpenhosDaas;
use App\Models\Daas\AcessaTG\NaturezaSubitensDaas;
use App\Models\Daas\AcessaTG\OrgaosDaas;
use App\Models\Daas\AcessaTG\SaldosDaas;
use App\Models\Daas\AcessaTG\UnidadesDaas;
use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use App\Models\Orgao;
use App\Models\Planointerno;
use App\Models\Quantidadeextracao;
use App\Models\SaldoContabil;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AcessaTGDaasController extends Controller
{
    public function execJdbcClass(string $tipo, string $unidade = null, string $documento_ano = null, string $meses = null)
    {
        $base = new BaseController();
        $data_d1 = $base->retornaDataFormatoMaisOuMenosQtdTipo('Ymd', '-', '1', 'Days', date('Y-m-d'));
        $linhas = null;
        $retorno = '';

        $dado = $this->retornaDado($tipo, $unidade, $documento_ano, $meses, $data_d1);

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        $hoje_md = date('m-d');
        $hoje_a = date('Y');
        $array_datas = config('app.datas_carga_rps');
        $busca_array = array_search($hoje_md,$array_datas);

        if (!isset($busca_contagem->id) or (isset($busca_contagem->id) and $tipo == 'cargarestosapagar' and $busca_array !== false and substr($busca_contagem->dado,15,4) == $hoje_a)) {

            $comando = $this->carregaComandoJdbc($tipo, $unidade, $documento_ano, $meses, $data_d1);
            $path = config('app.java_jdbc_path');

            $retorno = trim(shell_exec("cd {$path} && {$comando}"));

            if (substr($retorno, 0, 14) == 'Linhas lidas: ') {
                $array = explode(': ', $retorno);
                $linhas = intval($array[1]);
            }


            if($linhas >= 0){
                $dado_contagem = [
                    'dado' => $dado,
                    'quantidade' => $linhas,
                ];
            }else{
                $dado_contagem = [
                    'dado' => 'ERRO|'.$dado,
                    'quantidade' => 0,
                ];
            }

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

        }

        return $retorno;
    }

    private function carregaComandoJdbc(string $tipo, string $unidade = null, string $documento_ano = null, string $meses = null, string $data_d1 = null)
    {
        $todaydate = date('Ymd');

        $parametros_comuns = [
            'DAAS_DB_DATABASE1' => env('DAAS_DB_DATABASE1'),
            'DAAS_DB_HOST1' => env('DAAS_DB_HOST1'),
            'DAAS_DB_PORT1' => env('DAAS_DB_PORT1'),
            "DAAS_DB_USERNAME1" => env('DAAS_DB_USERNAME1'),
            "DAAS_DB_PASSWORD1" => env('DAAS_DB_PASSWORD1'),
            "DAAS_DB_DELIMITER1" => env('DAAS_DB_DELIMITER1'),
        ];

        $parametros = [
            'PARAMETERS' => '',
            'JDBC_CLASS' => '',
        ];

        $command = config('app.java_jdbc_command');

        foreach ($parametros_comuns as $key => $value) {
            $command = str_replace($key, $value, $command);
        }

        if ($tipo == 'naturezasubitem' or $tipo == 'unidades' or $tipo == 'orgaos') {
            $parametros = [
                'PARAMETERS' => '-DtodayDate="' . $todaydate . '"',
                'JDBC_CLASS' => 'JdbcClientQuery' . ucfirst($tipo),
            ];
        }

        if (($tipo == 'cargasaldocontabil' or $tipo == 'd1saldocontabil') and $unidade != null and $documento_ano != null) {
            $contas_contabeis = implode(',', config('app.saldo_contas_contabeis'));
            $parametros = [
                'PARAMETERS' => '-DtodayDate="' . $todaydate . '" -DcontasContabeis="' . $contas_contabeis . '"  -Dunidade="' . $unidade . '" -DdocumentoAno="' . $documento_ano . '"',
                'JDBC_CLASS' => 'JdbcClientQuerySaldoContabil',
            ];
        }

        if ($tipo == 'cargaempenhos' and $unidade != null and $documento_ano != null) {
            $parametros = [
                'PARAMETERS' => '-Dunidade="' . $unidade . '" -DdocumentoAno="' . $documento_ano . '"',
                'JDBC_CLASS' => 'JdbcClientQueryCargaEmpenhos',
            ];
        }

        if ($tipo == 'cargarestosapagar' and $unidade != null and $documento_ano != null) {
            $parametros = [
                'PARAMETERS' => '-Dunidade="' . $unidade . '" -DdocumentoAno="' . $documento_ano . '"',
                'JDBC_CLASS' => 'JdbcClientQueryCargaRestosAPagar',
            ];
        }

        if ($tipo == 'd1empenhos' and $unidade != null and $documento_ano != null and $data_d1 != null) {
            $parametros = [
                'PARAMETERS' => '-DdataD1="' . $data_d1 . '"  -Dunidade="' . $unidade . '" -DdocumentoAno="' . $documento_ano . '"',
                'JDBC_CLASS' => 'JdbcClientQueryD1Empenhos',
            ];
        }

        if ($tipo == 'cargaitensempenhos' and $unidade != null and $documento_ano != null and $meses != null) {
            $parametros = [
                'PARAMETERS' => '-Dmeses="' . $meses . '"  -Dunidade="' . $unidade . '" -DdocumentoAno="' . $documento_ano . '"',
                'JDBC_CLASS' => 'JdbcClientQueryCargaItensEmpenhos',
            ];
        }

        if ($tipo == 'd1itensempenhos' and $unidade != null and $documento_ano != null and $data_d1 != null) {
            $parametros = [
                'PARAMETERS' => '-DdataD1="' . $data_d1 . '"  -Dunidade="' . $unidade . '" -DdocumentoAno="' . $documento_ano . '"',
                'JDBC_CLASS' => 'JdbcClientQueryD1ItensEmpenhos',
            ];
        }


        foreach ($parametros as $key => $value) {
            $command = str_replace($key, $value, $command);
        }

        return $command;

    }

    private function retornaDado(string $tipo, string $unidade = null, string $documento_ano = null, string $meses = null, string $data_d1 = null)
    {
        $dado = '';

        if ($tipo == 'unidades') {
            $dado = 'CARGA|UNIDADE|' . date('Ymd');
        }

        if ($tipo == 'orgaos') {
            $dado = 'CARGA|ORGAO|' . date('Ymd');
        }

        if ($tipo == 'naturezasubitem') {
            $dado = 'CARGA|NDSUBITENS|' . date('Ymd');
        }

        if ($tipo == 'cargasaldocontabil') {
            $dado = 'CARGA|SALDO|' . $unidade . "|" . $documento_ano;
        }

        if ($tipo == 'd1saldocontabil') {
            $dado = 'CARGA|SALDO|' . $unidade . "|" . $documento_ano . "|" . $data_d1;
        }

        if ($tipo == 'cargaempenhos') {
            $dado = 'CARGA|NE|' . $unidade . "|" . $documento_ano;
        }

        if ($tipo == 'cargarestosapagar') {
            $dado = 'CARGA|RP|' . $unidade . "|" . $documento_ano;
        }

        if ($tipo == 'd1empenhos') {
            $dado = 'INCREMENTAL|NE|' . $unidade . "|" . $documento_ano . "|" . $data_d1;
        }

        if ($tipo == 'cargaitensempenhos') {
            $dado = 'CARGA|ITENSNE|' . $unidade . "|" . $documento_ano . "|" . $meses;
        }

        if ($tipo == 'd1itensempenhos') {
            $dado = 'INCREMENTAL|ITENSNE|' . $unidade . "|" . $documento_ano . "|" . $data_d1;
        }

        return $dado;
    }

    public function empenhosD1UnidadeAno(string $unidade, string $documento_ano)
    {
        $base = new BaseController();
        $data_d1 = $base->retornaDataFormatoMaisOuMenosQtdTipo('Ymd', '-', '1', 'Days', date('Y-m-d'));

        $dado = 'INCREMENTAL|NE|' . $unidade . "|" . $documento_ano . "|" . $data_d1;

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $empenhos = new EmpenhosDaas();
            $empenhos = $empenhos->buscaEmpenhosD1UnidadeAno($unidade, $documento_ano, $data_d1);

            $i = 0;
            foreach ($empenhos as $empenho) {

                switch ($empenho->tipo_favorecido) {
                    case 'PJ':
                        $tipo = "1";
                        break;
                    case 'PF':
                        $tipo = "2";
                        break;
                    case 'IC':
                        $tipo = "3";
                        break;
                    case 'UG':
                        $tipo = "4";
                        break;
                }

                $this->inserirCredor($empenho->favorecido, $empenho->nome_favorecido, $tipo);
                $this->inserirPlanoInterno($empenho->planointerno, $empenho->nome_planointerno);

                $dtEmissao = implode('-', array_reverse(explode('/', $empenho->emissao)));

                $emp = new EmpenhoController();
                $busca = $emp->buscaEmpenho($empenho->ug, $empenho->gestao, $empenho->numero);

                if (!isset($busca->numero)) {
                    $novo_empenho = new Empenho;
                    $novo_empenho->ug = $empenho->ug;
                    $novo_empenho->gestao = $empenho->gestao;
                    $novo_empenho->numero = $empenho->numero;
                    $novo_empenho->numero_ref = '';
                    $novo_empenho->emissao = $dtEmissao;
                    $novo_empenho->tipofavorecido = $tipo;
                    $novo_empenho->favorecido = $empenho->favorecido;
                    $novo_empenho->observacao = strtoupper(utf8_encode($empenho->observacao));
                    $novo_empenho->fonte = $empenho->fonte;
                    $novo_empenho->naturezadespesa = $empenho->naturezadespesa;
                    $novo_empenho->planointerno = $empenho->planointerno;
                    $novo_empenho->num_lista = $empenho->num_lista;
                    $novo_empenho->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Empenhos carregados.';

        } else {
            $ok = 'Empenhos carregados anteriomente!';
        }

        return $ok;

    }

    public function empenhosCargaUnidadeAno(string $unidade, string $documento_ano)
    {

        $dado = 'CARGA|NE|' . $unidade . "|" . $documento_ano;

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $empenhos = new EmpenhosDaas();
            $empenhos = $empenhos->buscaEmpenhosCargaUnidadeAno($unidade, $documento_ano);

            $i = 0;
            foreach ($empenhos as $empenho) {

                switch ($empenho->tipo_favorecido) {
                    case 'PJ':
                        $tipo = "1";
                        break;
                    case 'PF':
                        $tipo = "2";
                        break;
                    case 'IC':
                        $tipo = "3";
                        break;
                    case 'UG':
                        $tipo = "4";
                        break;
                }

                $this->inserirCredor($empenho->favorecido, $empenho->nome_favorecido, $tipo);
                $this->inserirPlanoInterno($empenho->planointerno, $empenho->nome_planointerno);

                $dtEmissao = implode('-', array_reverse(explode('/', $empenho->emissao)));

                $emp = new EmpenhoController();
                $busca = $emp->buscaEmpenho($empenho->ug, $empenho->gestao, $empenho->numero);

                if (!isset($busca->numero)) {
                    $novo_empenho = new Empenho;
                    $novo_empenho->ug = $empenho->ug;
                    $novo_empenho->gestao = $empenho->gestao;
                    $novo_empenho->numero = $empenho->numero;
                    $novo_empenho->numero_ref = '';
                    $novo_empenho->emissao = $dtEmissao;
                    $novo_empenho->tipofavorecido = $tipo;
                    $novo_empenho->favorecido = $empenho->favorecido;
                    $novo_empenho->observacao = strtoupper(utf8_encode($empenho->observacao));
                    $novo_empenho->fonte = $empenho->fonte;
                    $novo_empenho->naturezadespesa = $empenho->naturezadespesa;
                    $novo_empenho->planointerno = $empenho->planointerno;
                    $novo_empenho->num_lista = $empenho->num_lista;
                    $novo_empenho->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Empenhos carregados.';

        } else {

            $ok = 'Carga já realizada!';
        }

        return $ok;

    }

    public function itensEmpenhosD1UnidadeAno(string $unidade, string $documento_ano)
    {
        $base = new BaseController();
        $data_d1 = $base->retornaDataFormatoMaisOuMenosQtdTipo('Ymd', '-', '1', 'Days', date('Y-m-d'));

        $dado = 'INCREMENTAL|ITENSNE|' . $unidade . "|" . $documento_ano . "|" . $data_d1;

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();


        if (!isset($busca_contagem->id)) {

            $itensempenhos = new EmpenhosDaas();
            $itensempenhos = $itensempenhos->buscaItensEmpenhosD1UnidadeAno($unidade, $documento_ano, $data_d1);

            $i = 0;
            foreach ($itensempenhos as $item) {

                $emp = new EmpenhodetalhadoController();
                $busca = $emp->buscaLista(
                    substr($item->lista_item, 0, 6),
                    substr($item->lista_item, 6, 5),
                    $item->lista_item,
                    str_pad($item->numitem, 3, "0", STR_PAD_LEFT),
                    str_pad($item->subitem, 2, "0", STR_PAD_LEFT)
                );

                if (!isset($busca->ug)) {
                    $itenempenho = new Empenhodetalhado;
                    $itenempenho->ug = substr($item->lista_item, 0, 6);
                    $itenempenho->gestao = substr($item->lista_item, 6, 5);
                    $itenempenho->numeroli = $item->lista_item;
                    $itenempenho->numitem = str_pad($item->numitem, 3, "0", STR_PAD_LEFT);
                    $itenempenho->subitem = str_pad($item->subitem, 2, "0", STR_PAD_LEFT);
                    $itenempenho->quantidade = number_format($item->quantidade, 5, ".", '');
                    $itenempenho->valorunitario = number_format($item->valorunitario, 2, ".", '');
                    $itenempenho->valortotal = number_format($item->valortotal, 2, ".", '');
                    $itenempenho->descricao = strtoupper(utf8_encode($item->descricao));
                    $itenempenho->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Itens Empenhos carregados.';

        } else {

            $ok = 'Itens Empenhos carregados anteriomente!';

        }


        return $ok;

    }

    public function itensEmpenhosCargaUnidadeAno(string $unidade, string $documento_ano, string $meses)
    {

        $dado = 'CARGA|ITENSNE|' . $unidade . "|" . $documento_ano . "|" . $meses;

        $dado1 = 'CARGA|ITENSNE|' . $unidade . "|" . $documento_ano;

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->orWhere('dado', $dado1)
            ->first();

        if (!isset($busca_contagem->id)) {

            $itensempenhos = new EmpenhosDaas();
            $itensempenhos = $itensempenhos->buscaItensEmpenhosCargaUnidadeAno($unidade, $documento_ano, $meses);

            $i = 0;
            foreach ($itensempenhos as $item) {

                $emp = new EmpenhodetalhadoController();
                $busca = $emp->buscaLista(
                    substr($item->lista_item, 0, 6),
                    substr($item->lista_item, 6, 5),
                    $item->lista_item,
                    str_pad($item->numitem, 3, "0", STR_PAD_LEFT),
                    str_pad($item->subitem, 2, "0", STR_PAD_LEFT)
                );

                if (!isset($busca->ug)) {
                    $itenempenho = new Empenhodetalhado;
                    $itenempenho->ug = substr($item->lista_item, 0, 6);
                    $itenempenho->gestao = substr($item->lista_item, 6, 5);
                    $itenempenho->numeroli = $item->lista_item;
                    $itenempenho->numitem = str_pad($item->numitem, 3, "0", STR_PAD_LEFT);
                    $itenempenho->subitem = str_pad($item->subitem, 2, "0", STR_PAD_LEFT);
                    $itenempenho->quantidade = number_format($item->quantidade, 5, ".", '');
                    $itenempenho->valorunitario = number_format($item->valorunitario, 2, ".", '');
                    $itenempenho->valortotal = number_format($item->valortotal, 2, ".", '');
                    $itenempenho->descricao = strtoupper(utf8_encode($item->descricao));
                    $itenempenho->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Itens Empenhos carregados.';

        } else {

            $ok = 'Itens Empenhos carregados anteriomente!';

        }


        return $ok;

    }

    public function saldoContabilD1UnidadeAno(string $unidade, string $documento_ano)
    {
        $base = new BaseController();
        $data_d1 = $base->retornaDataFormatoMaisOuMenosQtdTipo('Ymd', '-', '1', 'Days', date('Y-m-d'));

        $dado = 'CARGA|SALDO|' . $unidade . "|" . $documento_ano . "|" . $data_d1;

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $saldos = new SaldosDaas();
            $saldos = $saldos->buscaSaldoContabilCargaUnidadeAno($unidade, $documento_ano);

            $i = 0;
            foreach ($saldos as $saldo) {

                $sld = new SaldoContabil();
                $busca = $sld->buscaSaldoContabilAno(
                    $saldo->ano,
                    $saldo->unidade,
                    str_pad($saldo->gestao, 5, "0", STR_PAD_LEFT),
                    $saldo->conta_contabil,
                    $saldo->conta_corrente
                );

                if (!isset($busca->unidade)) {
                    $saldocontabil = new SaldoContabil();
                    $saldocontabil->ano = $saldo->ano;
                    $saldocontabil->unidade = $saldo->unidade;
                    $saldocontabil->gestao = str_pad($saldo->gestao, 5, "0", STR_PAD_LEFT);
                    $saldocontabil->conta_contabil = $saldo->conta_contabil;
                    $saldocontabil->conta_corrente = $saldo->conta_corrente;
                    $saldocontabil->debito_inicial = number_format($saldo->debito_inicial, 2, '.', '');
                    $saldocontabil->credito_inicial = number_format($saldo->credito_inicial, 2, '.', '');
                    $saldocontabil->debito_mensal = number_format($saldo->debito_mensal, 2, '.', '');
                    $saldocontabil->credito_mensal = number_format($saldo->credito_mensal, 2, '.', '');
                    $saldocontabil->saldo = number_format($saldo->saldo, 2, '.', '');
                    $saldocontabil->tiposaldo = $saldo->tiposaldo;
                    $saldocontabil->save();
                } else {
                    $busca->debito_inicial = number_format($saldo->debito_inicial, 2, '.', '');
                    $busca->credito_inicial = number_format($saldo->credito_inicial, 2, '.', '');
                    $busca->debito_mensal = number_format($saldo->debito_mensal, 2, '.', '');
                    $busca->credito_mensal = number_format($saldo->credito_mensal, 2, '.', '');
                    $busca->saldo = number_format($saldo->saldo, 2, '.', '');
                    $busca->tiposaldo = $saldo->tiposaldo;
                    $busca->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Saldos carregados.';

        } else {

            $ok = 'Saldos carregados anteriomente!';

        }


        return $ok;

    }

    public function saldoContabilCargaUnidadeAno(string $unidade, string $documento_ano)
    {
        $dado = 'CARGA|SALDO|' . $unidade . "|" . $documento_ano;

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $saldos = new SaldosDaas();
            $saldos = $saldos->buscaSaldoContabilCargaUnidadeAno($unidade, $documento_ano);

            $i = 0;
            foreach ($saldos as $saldo) {

                $sld = new SaldoContabil();
                $busca = $sld->buscaSaldoContabilAno(
                    $saldo->ano,
                    $saldo->unidade,
                    str_pad($saldo->gestao, 5, "0", STR_PAD_LEFT),
                    $saldo->conta_contabil,
                    $saldo->conta_corrente
                );

                if (!isset($busca->unidade)) {
                    $saldocontabil = new SaldoContabil();
                    $saldocontabil->ano = $saldo->ano;
                    $saldocontabil->unidade = $saldo->unidade;
                    $saldocontabil->gestao = str_pad($saldo->gestao, 5, "0", STR_PAD_LEFT);
                    $saldocontabil->conta_contabil = $saldo->conta_contabil;
                    $saldocontabil->conta_corrente = $saldo->conta_corrente;
                    $saldocontabil->debito_inicial = number_format($saldo->debito_inicial, 2, '.', '');
                    $saldocontabil->credito_inicial = number_format($saldo->credito_inicial, 2, '.', '');
                    $saldocontabil->debito_mensal = number_format($saldo->debito_mensal, 2, '.', '');
                    $saldocontabil->credito_mensal = number_format($saldo->credito_mensal, 2, '.', '');
                    $saldocontabil->saldo = number_format($saldo->saldo, 2, '.', '');
                    $saldocontabil->tiposaldo = $saldo->tiposaldo;
                    $saldocontabil->save();
                } else {
                    $busca->debito_inicial = number_format($saldo->debito_inicial, 2, '.', '');
                    $busca->credito_inicial = number_format($saldo->credito_inicial, 2, '.', '');
                    $busca->debito_mensal = number_format($saldo->debito_mensal, 2, '.', '');
                    $busca->credito_mensal = number_format($saldo->credito_mensal, 2, '.', '');
                    $busca->saldo = number_format($saldo->saldo, 2, '.', '');
                    $busca->tiposaldo = $saldo->tiposaldo;
                    $busca->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Saldos carregados.';

        } else {

            $ok = 'Saldos carregados anteriomente!';

        }
        return $ok;

    }

    public function orgaos()
    {
        $dado = 'CARGA|ORGAO|' . date('Ymd');

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $orgaos = new OrgaosDaas();
            $orgaos = $orgaos->buscaOrgaos();

            $i = 0;
            foreach ($orgaos as $orgao) {

                $org = new OrgaoController();
                $busca = $org->buscaOrgao($orgao->codigo);

                if (!isset($busca->codigo)) {
                    $novo_orgao = new Orgao;
                    $novo_orgao->orgao_superior = $orgao->orgao_superior;
                    $novo_orgao->codigo = $orgao->codigo;
                    $novo_orgao->gestao = $orgao->gestao;
                    $novo_orgao->nome = utf8_encode($orgao->nome);
                    $novo_orgao->save();
                } else {
                    $busca->orgao_superior = $orgao->orgao_superior;
                    $busca->gestao = $orgao->gestao;
                    $busca->nome = utf8_encode($orgao->nome);
                    $busca->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Orgaos carregados.';

        } else {

            $ok = 'Orgaos carregados anteriomente!';

        }
        return $ok;
    }

    public function unidades()
    {
        $dado = 'CARGA|UNIDADE|' . date('Ymd');

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $unidades = new UnidadesDaas();
            $unidades = $unidades->buscaUnidades();

            $i = 0;
            foreach ($unidades as $unidade) {

                $uni = new UnidadeController();
                $busca = $uni->buscaUnidade($unidade->codigo);

                if (!isset($busca->codigo)) {
                    $nova_unidade = new Unidade;
                    $nova_unidade->codigo = $unidade->codigo;
                    $nova_unidade->cnpj = $unidade->cnpj;
                    $nova_unidade->funcao = $unidade->funcao;
                    $nova_unidade->nome = utf8_encode($unidade->nome);
                    $nova_unidade->nomeresumido = utf8_encode($unidade->nomeresumido);
                    $nova_unidade->uf = $unidade->uf;
                    $nova_unidade->orgao = $unidade->orgao;
                    $nova_unidade->save();
                } else {
                    $busca->cnpj = $unidade->cnpj;
                    $busca->funcao = $unidade->funcao;
                    $busca->nome = utf8_encode($unidade->nome);
                    $busca->nomeresumido = utf8_encode($unidade->nomeresumido);
                    $busca->uf = $unidade->uf;
                    $busca->orgao = $unidade->orgao;
                    $busca->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Unidades carregadas.';

        } else {

            $ok = 'Unidades carregadas anteriomente!';

        }
        return $ok;
    }

    public function naturezaSubitens()
    {
        $dado = 'CARGA|NDSUBITENS|' . date('Ymd');

        $busca_contagem = Quantidadeextracao::where('dado', $dado)
            ->first();

        if (!isset($busca_contagem->id)) {

            $nd = new NaturezaSubitensDaas();
            $ndsubitens = $nd->buscaNaturezaSubitens();

            $i = 0;
            foreach ($ndsubitens as $ndsubitem) {

                $busca = Naturezasubitens::where('codigo_nd', $ndsubitem->codigo_nd)
                    ->where('codigo_subitem', str_pad($ndsubitem->codigo_subitem, 2, "0", STR_PAD_LEFT))
                    ->first();

                if (!isset($busca->codigo_nd)) {
                    $nova_ndsubitem = new Naturezasubitens();
                    $nova_ndsubitem->codigo_nd = $ndsubitem->codigo_nd;
                    $nova_ndsubitem->descricao_nd = utf8_encode($ndsubitem->descricao_nd);
                    $nova_ndsubitem->codigo_subitem = str_pad($ndsubitem->codigo_subitem, 2, "0", STR_PAD_LEFT);
                    $nova_ndsubitem->descricao_subitem = utf8_encode($ndsubitem->descricao_subitem);
                    $nova_ndsubitem->save();
                } else {
                    $busca->descricao_nd = utf8_encode($ndsubitem->descricao_nd);
                    $busca->descricao_subitem = utf8_encode($ndsubitem->descricao_subitem);
                    $busca->save();
                }

                $i++;
            }

            $dado_contagem = [
                'dado' => $dado,
                'quantidade' => $i,
            ];

            $qtdextracao = new Quantidadeextracao();
            $contagem = $qtdextracao->inserirDado($dado_contagem);

            $ok = 'Natureza e Subitens carregados.';

        } else {

            $ok = 'Natureza e Subitens carregados anteriomente!';

        }
        return $ok;
    }

    public function inserirCredor(string $cpf_cnpj_idgener, string $nome, string $tipo)
    {
        if ($tipo != 4) {
            $cpf_cnpj_idgener = $this->formataCnpjCpfTipo($cpf_cnpj_idgener, $tipo);

            $busca = Credor::where('cpf_cnpj_idgener', $cpf_cnpj_idgener)
                ->first();

            if (!isset($busca->id)) {
                $credor = new Credor();
                $credor->tipofornecedor = $tipo;
                $credor->cpf_cnpj_idgener = $cpf_cnpj_idgener;
                $credor->nome = utf8_encode($nome);
                $credor->uf = '99';
                $credor->save();
            } else {
                $busca->tipofornecedor = $tipo;
                $busca->nome = utf8_encode($nome);
                $busca->save();
            }

        }
    }


    public function inserirPlanoInterno(string $codigo, string $descricao)
    {

        $busca = Planointerno::where('codigo', $codigo)
            ->first();

        if (!isset($busca->id)) {
            $planointerno = new Planointerno();
            $planointerno->codigo = $codigo;
            $planointerno->descricao = utf8_encode($descricao);
            $planointerno->save();
        } else {
            $busca->descricao = utf8_encode($descricao);
            $busca->save();
        }

    }


}
